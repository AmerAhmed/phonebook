<?php require_once( dirname(__FILE__) . "/common/my-config.php"); ?>
<link rel="stylesheet" href="css/bootstrap.min.css"/>
<link rel="stylesheet" href="css/fontawesome-all.min.css"/>
<link rel="stylesheet" href="css/jquery.dataTables.min.css"/>

<div class="container">
	<a class="btn btn-outline-info mt-3" href="add.php">New</a>
	<div class="row py-5">
		<div class="col-md-12">
			<table class="table table-striped table-bordered mt-5" id="myTable">
				<thead>
					<tr>
						<th>Name</th>
						<th>Email</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php
						$contacts = mysqli_query($conn, ' select * from `contact` ');
						while ($row = mysqli_fetch_array($contacts)) {
							foreach ($row as $key => $val) {
								$row[$key] = mysqli_real_escape_string($conn, $row[$key]);
							}
							echo('<tr>');
							echo('<td>'.$row['name'].'</td>');
							echo('<td>'.$row['email'].'</td>');
							echo('<td> <a href="edit.php?id='.$row['id'].'"><i class="fa fa-edit"></i></a> <a href="controllers/delete-contact.php?id='.$row['id'].'"><i class="fas fa-trash-alt" style="color:red;"></i></a>');
							echo('</td>');
							echo('</tr>');
						}
					?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<script src="js/jquery-3.3.1.min.js"></script> 
<script src="js/jquery.dataTables.min.js"></script>

<script type="text/javascript">
	$(document).ready( function () {
    	$('#myTable').DataTable();
	});
</script> 

<style type="text/css">
	a
	{
		text-decoration: none !important;
	}
</style>