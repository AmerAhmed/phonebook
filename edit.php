<?php 
  require_once( dirname(__FILE__) . "/common/my-config.php");
  $id = $conn->escape_string($_GET['id']);
  $contact = mysqli_query($conn, ' select * from `contact` where id = '.$id);
  while($result = mysqli_fetch_array($contact))
  {
    $name = $result['name'];
    $email = $result['email'];
  }
?>

<link rel="stylesheet" href="css/bootstrap.min.css"/>
<link rel="stylesheet" href="css/fontawesome-all.min.css"/>

<div class="container">
  <a class="btn btn-outline-info mt-3" href="index.php">Back</a>
  <form method="post" action="controllers/edit-contact.php" class="m-auto w-100 py-3" id="edit-contact">
    <input type="hidden" name="id" value="<?php echo $id; ?>" />
    <div class="form-group">
        <label>Name :</label>
        <input class="form-control" type="text" name="Contact[name]" value="<?=$name?>" />
    </div>
    <div class="form-group">
        <label>E-mail :</label>
        <input class="form-control" type="email" name="Contact[email]" value="<?=$email?>" />
    </div>
    <button type="submit" class="btn btn-primary float-right">Update</button>
  </form>
</div>

<script src="js/jquery-3.3.1.min.js"></script>        
<script src="js/validation/jquery.validate.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
      $("#edit-contact").validate({    
          rules: {
              'Contact[name]':'required',
              'Contact[email]':{
                required:true,
                email:true,
              },
          }
      });
  });
</script>

<style type="text/css">
    .error 
    {
      border-color: red !important;
      color:red !important; 
      display: block;
    }
    a
    {
      text-decoration: none !important;
    }
</style>